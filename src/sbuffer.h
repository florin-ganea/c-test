/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SBUFFER_H
#define SBUFFER_H

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#ifdef __cplusplus
extern "C" {
#endif

 /** String copy */
#define safestrcpy(dst, src, len) \
    { \
        size_t dlen = sizeof(dst); /* destination size */ \
        size_t alen = len; /* actual lenght to be considered */ \
        if (dlen != sizeof(void*)) { /* dst is char[], hence dlen is the memory size */ \
            if (dlen - 1 < alen) { /* bad - memory size is less than the provided len */ \
                alen = dlen - 1; \
                /* log overflow */ printf("safestrcpy overflow "); \
            } \
        } \
        strncpy( (dst), (src), alen ); \
        (dst)[alen] = '\0'; \
    }

#define safestrcat(dst, src, len) \
    { \
        size_t slen = strlen(dst); /* destination string length */ \
        size_t dlen = sizeof(dst); /* destination size */ \
        size_t alen = len; /* actual lenght to be considered */ \
        if (dlen != sizeof(void*)) { /* dst is char[], hence dlen is the memory size */ \
            if (dlen - 1 < alen) { /* bad - memory size is less than the provided len */ \
                alen = dlen - 1; \
                /* log overflow */ printf("safestrcat overflow "); \
            } \
        } \
        if (slen < alen) { /* more space available in the destination */ \
            strncpy( (dst) + slen, (src), alen - slen ); \
            (dst)[alen] = '\0'; \
        } else { \
            /* log overflow */ printf("safestrcat overflow 2 "); \
        } \
    }

#define safestrcatf(dst, len, fmt, ...) \
    { \
        size_t slen = strlen(dst); /* destination string length */ \
        size_t dlen = sizeof(dst); /* destination size */ \
        size_t alen = len; /* actual lenght to be considered */ \
        if (dlen != sizeof(void*)) { /* dst is char[], hence dlen is the memory size */ \
            if (dlen - 1 < alen) { /* bad - memory size is less than the provided len */ \
                alen = dlen - 1; \
                /* log overflow */ printf("safestrcatf overflow "); \
            } \
        } \
        if (slen < alen) { /* more space available in the destination */ \
            int __rc_sscf = snprintf( (dst) + slen, alen - slen + 1, fmt, ##__VA_ARGS__ ); \
            if (__rc_sscf < 0) { \
                /* log error */ printf("safestrcatf error"); \
            } else if ((size_t)__rc_sscf >= alen - slen + 1) { \
                /* log overflow */ printf("safestrcatf overflow 3 "); \
            } \
            (dst)[alen] = '\0'; \
        } else { \
            /* log overflow */ printf("safestrcatf overflow 2 "); \
        } \
    }

typedef struct string_buffer {
    char* buffer;
    size_t allocated_size;
    size_t current_size;
} string_buffer_t;

string_buffer_t* string_buffer_alloc(size_t allocated_size, const char* initial_value);
int string_buffer_append_string(string_buffer_t* sbuffer, const char* value);
int string_buffer_append(string_buffer_t* sbuffer, const char* format, ...) __attribute__((format(printf, 2, 3)));
int string_buffer_free(string_buffer_t** psbuffer);

#define sb_wrap(buff, size, autoflush) \
    string_buffer_t* __fp_##buff = string_buffer_alloc(size, ""); \
    int __rc_##buff; \
    int __err_##buff = 0;

#define sb_appendf(buff, fmt, ...) \
    __rc_##buff = string_buffer_append(__fp_##buff, fmt, ##__VA_ARGS__); \
    if (__rc_##buff < 0) { \
        __err_##buff = ENOBUFS; \
    } else { \
        __err_##buff = 0; \
    }

/* set to ENOBUFS in case there is a buffer overflow;
 * autoflush flag set determines that overflow is not reported */
#define sb_errno(buff) \
    __err_##buff

#define sb_end(buff) \
    string_buffer_free(&__fp_##buff);

#ifdef __cplusplus
}
#endif

#endif /* SBUFFER_H */
