/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TRY_CATCH_H
#define TRY_CATCH_H

#include <setjmp.h>
#include <stdlib.h>

#include "assert.h"

#ifdef __cplusplus
extern "C" {
#endif

jmp_buf* env_create_tl();
jmp_buf* env_remove_tl();
    
// -- exception handling macros --

/* the thrown exception, available inside a TRY-CACTH block */
#define THROWN __jmp_rc


#define THROW(exception) \
    { \
        jmp_buf* penv = env_remove_tl(); \
        if (penv == NULL) { /* unhandled in an enclosing try */ \
            __error("uncaught exception: %d, literal " #exception "; aborting", exception) \
            abort(); \
        } \
        longjmp(*penv, exception); \
    }

/* begins a TRY-CATCH block */
#define TRY \
    { \
        jmp_buf* __jmp_env = env_create_tl(); \
        __assert_not_null(__jmp_env); /* should not happen */ \
        int THROWN = setjmp(*__jmp_env); \
        \
        switch (THROWN) { \
            case 0: \
                { \
                    /* normal code follows */

/* handling a specific exception */
#define CATCH(exception) \
                } \
                break; \
            case exception: \
                { \
                    free(__jmp_env); /* free to allow throw inside catch */ \
                    __jmp_env = NULL; \
                    /* exception handling code follows */

/* handling other, previously unhandled, exception */
#define CATCH_OTHER \
                } \
                break; \
            default: \
                { \
                    free(__jmp_env); /* free to allow throw inside catch */ \
                    __jmp_env = NULL; \
                    /* default exception handling code follows */

/* ends a TRY-CATCH block */
#define TRY_END \
                } \
                break; \
        } \
        if (THROWN == 0) { \
            /* no exception thrown; remove the top env; the same reference */ \
            __jmp_env = env_remove_tl(); \
            free(__jmp_env); \
            __jmp_env = NULL; \
        } else { \
            if (__jmp_env != NULL) { \
                free(__jmp_env); \
                __jmp_env = NULL; \
                /* throw unhandled exceptions, and abort on top TRY-CATCH */ \
                THROW(THROWN); \
            } \
        } \
    }


#ifdef __cplusplus
}
#endif

#endif /* TRY_CATCH_H */

