/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum __level {
    OFF,
    ERROR,
    WARNING,
    INFO,
    DEBUG,
    TRACE,
    ALWAYS
} __level_t;

#ifndef LOG_SEP
#define LOG_SEP " "
#endif

#ifndef LOG_EOL
#define LOG_EOL "\n"
#endif

#ifndef LOG_INDENT
#define LOG_INDENT "    "
#endif

#define __debug(message, ...) \
    __log(stdout, DEBUG, NULL, __FILE__, __LINE__, 0, message, ##__VA_ARGS__);

#define __error(message, ...) \
    __log(stderr, ERROR, NULL, __FILE__, __LINE__, 0, message, ##__VA_ARGS__);

void __log(FILE* storage, __level_t level, const char* logger, const char* src, int line, int indent, const char* fmt, ...)
        __attribute__((format(printf, 7, 8)));

#ifdef __cplusplus
}
#endif

#endif /* LOGGING_H */
