/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "sbuffer.h"

string_buffer_t* string_buffer_alloc(size_t allocated_size, const char* initial_value)
{
    string_buffer_t* sbuffer = (string_buffer_t*) malloc(sizeof(string_buffer_t));
    sbuffer->buffer = (char*) malloc(allocated_size * sizeof(char));
    sbuffer->allocated_size = allocated_size;
    sbuffer->current_size = 0;
    sbuffer->buffer [sbuffer->current_size] = '\0';

    string_buffer_append_string(sbuffer, initial_value);

    return sbuffer;
}

int string_buffer_append_string(string_buffer_t* sbuffer, const char* value)
{
   strncpy(sbuffer->buffer + sbuffer->current_size, value, sbuffer->allocated_size - sbuffer->current_size);
   return 0;
}

int string_buffer_append(string_buffer_t* sbuffer, const char* format, ...)
{
    va_list va;
    va_start(va, format);

    vsnprintf(sbuffer->buffer + sbuffer->current_size, sbuffer->allocated_size - sbuffer->current_size, format, va);

    va_end(va);

   return 0;
}

int string_buffer_free(string_buffer_t** psbuffer)
{
    if (*psbuffer != NULL) {
        if ((*psbuffer)->buffer != NULL) {
            free((*psbuffer)->buffer);
            (*psbuffer)->buffer = NULL;
        }
        free(*psbuffer);
        *psbuffer = NULL;
    }
    return 0;
}
