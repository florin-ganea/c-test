/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

#include "run_test.h"

#include "bitop.h"
#include "sbuffer.h"

extern const char *h_date_fmt;
extern const char *h_datetime_fmt;

void print_addrs();

typedef enum colors {
    RED,
    GREEN,
    BLUE
} colors_t;

const char* COMPLETION_STATUSES[] = {"COMPLETED_YES", "COMPLETED_NO", "COMPLETED_MAYBE"};


// SIGSEGV handler

sigjmp_buf __jenv;

void sigsegv_handler()
{
   siglongjmp(__jenv, SIGSEGV);
}

void signal_init(void )
{

    struct sigaction sih;

    sih.sa_handler = sigsegv_handler;
    sigemptyset(&sih.sa_mask);
    sih.sa_flags = 0;
    sigaction(SIGSEGV, &sih, NULL);

}


//
// -- missing include
//
int test_missing_include()
{
    int correct = 1;
    int result;

    char test[] = "test";

    result = isascii(test);
    correct = (result != 0);

    printf("isascii(%s) = %d", test, result);

    return correct;
}

// Fails when -fshort-enums
int test_enum()
{
    size_t i;
    int correct = 1;
    int* ia;
    colors_t* ca = (colors_t*) malloc(10 * sizeof(colors_t));

    for (i = 0; i < 10; i++)
        ca[i] = GREEN;

    ia = (int*) ca;
    ia[0] = BLUE;

    for (i = 0; i < 10; i++)
        correct = correct && ((i == 0 && ia[i] == BLUE) || ia[i] == GREEN);

    PRINT_ARRAY(ca, 10, "%d")

    free(ca);

    return correct;
}

int test_format_string_ok()
{
    int correct = 1;
    char outb[1024];

    char test[] = "test";
    long val = 1234567891234L;
    int vali = -123;

    snprintf(outb, 1024, "%ld %d %s", val, vali, test);
    correct = strcmp("1234567891234 -123 test", outb) == 0;

    printf("outb = %s", outb);

    return correct;
}

int test_format_string_somewhat_bad()
{
    int correct = 1;
    char outb[1024];

    char test[] = "test";
    long val = 1234567891234L;
    int vali = -123;

    snprintf(outb, 1024, "%d %ld %s", val, vali, test);
    correct = strcmp("1234567891234 -123 test", outb) == 0;

    printf("outb = %s", outb);

    return correct;
}

int test_format_string_bad()
{
    int correct = 1;
    char outb[1024];

    char test[] = "test";
    long val = 1234567891234L;
    int vali = -123;

    snprintf(outb, 1024, "%d %f %s", val, vali, test);
    correct = strcmp("1234567891234 -123 test", outb) == 0;

    printf("outb = %s", outb);

    return correct;
}

int test_long_endian()
{
    int correct = 1;
    size_t llen = sizeof(long);

    long hval = 0x0102030405060708;
    long nval;
    char* pnval = (char*) &nval;
    char buff[llen];

    nval = htonl(hval);

    memcpy(buff, pnval, llen);
    //PRINT_ARRAY(pnval, llen, "0x%02X") printf(" ");

    nval = 0;
    //PRINT_ARRAY(pnval, llen, "0x%02X") printf(" ");
    memcpy(pnval, buff, llen);
    //PRINT_ARRAY(pnval, llen, "0x%02X") printf(" ");

    correct = hval == ntohl(nval);

    PRINT_ARRAY(buff, llen, "0x%02X") printf(" ");
    printf("hval = 0x%016lX, nval = 0x%016lX", (unsigned long) hval, (unsigned long) nval);

    return correct;
}

int test_long_to_int()
{
    int correct = 1;
    long lval;
    int val;

    lval = 0x0102030405060708L;

    val = lval;

    correct = (val == lval);

    printf("lval = 0x%016lX, val = 0x%08X", (unsigned long) lval, (unsigned int) val);

    return correct;
}

int test_bit_operations()
{
    int correct = 1;

    char ch;
    unsigned char uch;
    short sh;
    unsigned short ush;
    int i;
    unsigned int ui;
    long l;
    unsigned long ul;
    long long ll;
    unsigned long long ull;

    ch = (char) 0x12;
    ch = char_lsh(ch, 2);
    correct = correct && ch == (char) 0x48;
    printf("ch = 0x%02hhX, ", (unsigned char) ch);

    ch = (char) 0x81;
    ch = char_lsh(ch, 2);
    correct = correct && ch == (char) 0x04;
    printf("ch = 0x%02hhX, ", (unsigned char) ch);

    ch = (char) 0x12;
    ch = char_ursh(ch, 2);
    correct = correct && ch == (char) 0x04;
    printf("ch = 0x%02hhX, ", (unsigned char) ch);

    ch = (char) 0x81;
    ch = char_ursh(ch, 2);
    correct = correct && ch == (char) 0x20;
    printf("ch = 0x%02hhX, ", (unsigned char) ch);

    return correct;
}

int test_extern()
{
    int correct = 1;

    print_addrs();
    printf("%p, %p\n", h_date_fmt, h_datetime_fmt);

    correct = h_date_fmt != NULL;
    printf("h_date_fmt = %s, ", h_date_fmt == NULL ? "(null)" : h_date_fmt);

    correct = correct && h_datetime_fmt != NULL;
    printf("h_datetime_fmt = %s", h_datetime_fmt == NULL ? "(null)" : h_datetime_fmt);

    return correct;
}

int test_int_to_char()
{
    static char key[] = {0x93, 0xDB, 0xA3, 0x34, 0x5E, 0x9A, 0x96, 0xB5};
    int ikey[] = {0x93, 0xDB, 0xA3, 0x34, 0x5E, 0x9A, 0x96, 0xB5};
    int correct = 1;
    size_t i;

    for (i = 0; i < sizeof(key); i++) {
        correct = correct && key[i] == ikey[i];
        printf("0x%02x vs. 0x%02hhx ", (unsigned int) key[i], (unsigned char) key[i]);
    }

    return correct;
}

int test_char_int_compare()
{
    //data[i] != 0xFF
    char key[] = "abcdef";
    int correct = 1;
    size_t i;

    for (i = 0; i < sizeof(key); i++) {
        correct = correct && (key[i] != 0xFF); // solution -- (char)0xFF || unsigned char[]
    }

    printf("0x%X was %sfound, ", (unsigned int) 0xFF, correct ? "not " : "");

    key[2] = 0xFF;

    correct = (key[2] == 0xFF);

    printf("0xFF was %sfound on index 2 (0x%X)", correct ? "" : "not ", (unsigned int) key[2]);

    return correct;
}

int test_sprintf_overlap()
{
    char buff[1024] = "";
    int correct = 1;

    sprintf(buff, "test %02d", 1);
    correct = strcmp(buff, "test 01") == 0;
    printf("'%s', ", buff);

    sprintf(buff, "%s test %02d", buff, 2);
    correct = strcmp(buff, "test 01 test 02") == 0;
    printf("'%s', ", buff);

    return correct;
}

int test_snprintf_overlap()
{
    char buff[1024] = "";
    int correct = 1;

    snprintf(buff, 1024, "test %02d", 1);
    correct = strcmp(buff, "test 01") == 0;
    printf("'%s', ", buff);

    snprintf(buff, 1024, "%s test %02d", buff, 2);
    correct = strcmp(buff, "test 01 test 02") == 0;
    printf("'%s', ", buff);

    return correct;
}

int test_sbuffer()
{
    char buff[1024] = "";
    sb_wrap(buff, 1024, 1);
    int correct = 1;

    sb_appendf(buff, "test %02d", 1);
    correct = strcmp(buff, "test 01") == 0;
    printf("'%s', ", buff);

    sb_appendf(buff, " test %02d", 2);
    correct = strcmp(buff, "test 01 test 02") == 0;
    printf("'%s', ", buff);

    sb_end(buff);

    return correct;
}

int test_sbuffer_ptr()
{
    char* buff = (char*) malloc(1024 * sizeof(char));
    sb_wrap(buff, 1024, 1);
    int correct = 1;

    sb_appendf(buff, "test %02d", 1);
    correct = strcmp(buff, "test 01") == 0;
    printf("'%s', ", buff);

    sb_appendf(buff, " test %02d", 2);
    correct = strcmp(buff, "test 01 test 02") == 0;
    printf("'%s', ", buff);

    sb_end(buff);
    free(buff);

    return correct;
}

int test_sbuffer_overflow()
{
    char buff[1024] = "";
    sb_wrap(buff, 1024, 0);
    int correct = 1;
    int i;

    for (i = 0; i < 1000; i++) {
        sb_appendf(buff, "test %02d ", i);
        if (sb_errno(buff) == ENOBUFS) {
            printf("overflow ");
            break;
        }
    }

    sb_end(buff);
    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (sb_errno(buff) == ENOBUFS);
    printf("sb_errno = %d - %s ", sb_errno(buff), strerror(sb_errno(buff)));

    return correct;
}

int test_safestrcpy()
{
    char buff[1024] = "";
    int correct = 1;

    safestrcpy(buff, "test", 2048);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test") == 0);

    return correct;
}

int test_safestrcpy_ptr()
{
    char* buff = (char*) malloc(1024 * sizeof(char));
    int correct = 1;

    safestrcpy(buff, "test", 2048);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test") == 0);
    free(buff);

    return correct;
}

int test_safestrcpy_ptrcpy()
{
    char buff[1024] = "";
    char* buffcpy = buff;
    int correct = 1;

    safestrcpy(buffcpy, "test", 2048);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test") == 0);

    return correct;
}

int test_safestrcat()
{
    char buff[1024] = "";
    int correct = 1;

    safestrcpy(buff, "test", 2048);
    safestrcat(buff, " another", 2048);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test another") == 0);

    return correct;
}

int test_safestrcat_overflow_2()
{
    char buff[10] = "";
    int correct = 1;

    safestrcpy(buff, "test", 9);
    safestrcat(buff, " another", 9);
    safestrcat(buff, " yet another", 9);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test anot") == 0);

    return correct;
}

int test_safestrcatf()
{
    char buff[1024] = "";
    int correct = 1;

    safestrcpy(buff, "test", 2048);
    safestrcatf(buff, 2048, " another");
    safestrcatf(buff, 2048, "#%d", 1);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test another#1") == 0);

    return correct;
}

int test_safestrcatf_overflow_2()
{
    char buff[10] = "";
    int correct = 1;

    safestrcpy(buff, "test", 9);
    safestrcatf(buff, 9, " another");
    safestrcatf(buff, 9, "#%d", 1);

    printf("buff (%zd) = '%s' ", strlen(buff), buff);

    correct = (strcmp(buff, "test anot") == 0);

    return correct;
}

int test_initializer()
{
    int correct = 1;

    correct = correct && strcmp("COMPLETED_YES", COMPLETION_STATUSES[0]) == 0;
    correct = correct && strcmp("COMPLETED_NO", COMPLETION_STATUSES[1]) == 0;
    correct = correct && strcmp("COMPLETED_MAYBE", COMPLETION_STATUSES[2]) == 0;

    return correct;
}

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int test_pthread_mutex_static_initializer()
{
    int correct = 1;

    correct = pthread_mutex_lock(&mutex) == 0;
    correct = correct && (pthread_mutex_unlock(&mutex) == 0);

    return correct;
}

pthread_mutex_t mutex_2 = PTHREAD_MUTEX_INITIALIZER;
int test_pthread_mutex_static_initializer_with_destroy()
{
    int correct = 1;

    correct = pthread_mutex_lock(&mutex_2) == 0;
    correct = correct && (pthread_mutex_unlock(&mutex_2) == 0);

    correct = correct && (pthread_mutex_destroy(&mutex_2) == 0);

    return correct;
}

/*
 *
 */
int main(int argc __attribute((unused)), char** argv __attribute((unused)))
{
    signal_init();

    main_01(argc, argv);

    RUN_TEST(test_missing_include)

    RUN_TEST(test_enum)

    RUN_TEST(test_format_string_ok)
    RUN_TEST(test_format_string_somewhat_bad)
    RUN_TEST(test_format_string_bad) // here core dump is expected

    RUN_TEST(test_long_endian)
    RUN_TEST(test_long_to_int)

    RUN_TEST(test_bit_operations)
    RUN_TEST(test_extern)

    RUN_TEST(test_int_to_char)
    RUN_TEST(test_char_int_compare)

    RUN_TEST(test_sprintf_overlap)
    RUN_TEST(test_snprintf_overlap)
    RUN_TEST(test_sbuffer)
    RUN_TEST(test_sbuffer_ptr)
    RUN_TEST(test_sbuffer_overflow)
    RUN_TEST(test_safestrcpy)
    RUN_TEST(test_safestrcpy_ptr) // here an ugly core dump is expected
    RUN_TEST(test_safestrcpy_ptrcpy) // here an ugly core dump is expected
    RUN_TEST(test_safestrcat)
    RUN_TEST(test_safestrcatf)
    RUN_TEST(test_safestrcat_overflow_2)
    RUN_TEST(test_safestrcatf_overflow_2)
    RUN_TEST(test_initializer)

    RUN_TEST(test_pthread_mutex_static_initializer)
    RUN_TEST(test_pthread_mutex_static_initializer_with_destroy)

    return (EXIT_SUCCESS);
}
