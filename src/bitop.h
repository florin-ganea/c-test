/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BITOP_H
#define BITOP_H

#ifdef __cplusplus
extern "C" {
#endif

#define __rsh_ext(type_result, type_shift, var, n) (type_result)((type_shift) var >> n)
#define __lsh_ext(type_result, type_shift, var, n) (type_result)((type_shift) var << n)

#define __and_ext(type_result, type_op, var_l, var_r) (type_result)((type_op) var_l & (type_op) var_r)
#define __or_ext(type_result, type_op, var_l, var_r) (type_result)((type_op) var_l | (type_op) var_r)
#define __xor_ext(type_result, type_op, var_l, var_r) (type_result)((type_op) var_l ^ (type_op) var_r)
#define __not_ext(type_result, type_op, var) (type_result)(~((type_op) var))

#define __ursh(type, var, n)        __rsh_ext(type, unsigned type, var, n)
#define __rsh(type, var, n)         __rsh_ext(type, type, var, n)
#define __lsh(type, var, n)         __lsh_ext(type, type, var, n)

#define __and(type, var_l, var_r)   __and_ext(type, type, var_l, var_r)
#define __or(type, var_l, var_r)    __or_ext(type, type, var_l, var_r)
#define __xor(type, var_l, var_r)   __xor_ext(type, type, var_l, var_r)
#define __not(type, var)            __not_ext(type, type, var)

// bitwise operations

#define char_and(val_l, val_r)      __and(char, val_l, val_r)
#define char_or(val_l, val_r)       __or(char, val_l, val_r)
#define char_xor(val_l, val_r)      __xor(char, val_l, val_r)
#define char_not(val)               __not(char, val)

#define short_and(val_l, val_r)     __and(short, val_l, val_r)
#define short_or(val_l, val_r)      __or(short, val_l, val_r)
#define short_xor(val_l, val_r)     __xor(short, val_l, val_r)
#define short_not(val)              __not(short, val)

#define int_and(val_l, val_r)       __and(int, val_l, val_r)
#define int_or(val_l, val_r)        __or(int, val_l, val_r)
#define int_xor(val_l, val_r)       __xor(int, val_l, val_r)
#define int_not(val)                __not(int, val)

#define long_and(val_l, val_r)      __and(long, val_l, val_r)
#define long_or(val_l, val_r)       __or(long, val_l, val_r)
#define long_xor(val_l, val_r)      __xor(long, val_l, val_r)
#define long_not(val)               __not(long, val)

#define long_long_and(val_l, val_r) __and(long long, val_l, val_r)
#define long_long_or(val_l, val_r)  __or(long long, val_l, val_r)
#define long_long_xor(val_l, val_r) __xor(long long, val_l, val_r)
#define long_long_not(val)          __not(long long, val)

// left shift

#define char_lsh(ch_val, n)         __lsh(char, ch_val, n)
#define short_lsh(sh_val, n)        __lsh(short, sh_val, n)
#define int_lsh(i_val, n)           __lsh(int, i_val, n)
#define long_lsh(l_val, n)          __lsh(long, l_val, n)
#define long_long_lsh(ll_val, n)    __lsh(long long, ll_val, n)

// right shift

#define char_rsh(ch_val, n)         __rsh(char, ch_val, n)
#define short_rsh(sh_val, n)        __rsh(short, sh_val, n)
#define int_rsh(i_val, n)           __rsh(int, i_val, n)
#define long_rsh(l_val, n)          __rsh(long, l_val, n)
#define long_long_rsh(ll_val, n)    __rsh(long long, ll_val, n)

// unsigned right shift

#define char_ursh(ch_val, n)        __ursh(char, ch_val, n)
#define short_ursh(sh_val, n)       __ursh(short, sh_val, n)
#define int_ursh(i_val, n)          __ursh(int, i_val, n)
#define long_ursh(l_val, n)         __ursh(long, l_val, n)
#define long_long_ursh(ll_val, n)   __ursh(long long, ll_val, n)

#ifdef __cplusplus
}
#endif

#endif /* BITOP_H */
