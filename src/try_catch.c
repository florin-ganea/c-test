/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <pthread.h>
#include <setjmp.h>

#include "try_catch.h"
#include "assert.h"

// -- jump environment stack --

static pthread_key_t __jmp_stack_key;
static pthread_once_t __jmp_stack_key_init_once;

typedef struct __jmp_stack_node {
    jmp_buf* env;
    struct __jmp_stack_node* next;
} jmp_stack_t;

static void jmp_stack_push(jmp_stack_t** pstack, jmp_buf* env)
{
    struct __jmp_stack_node* pe;

    __assert_not_null(pstack);

    pe = (struct __jmp_stack_node*) malloc(sizeof(struct __jmp_stack_node));
    __assert_not_null(pe);

    pe->env = env;
    pe->next = *pstack;
    *pstack = pe;
}

static jmp_buf* jmp_stack_pop(jmp_stack_t** pstack)
{
    struct __jmp_stack_node* pe;
    jmp_buf* res = NULL;

    __assert_not_null(pstack);

    pe = *pstack;

    if (pe != NULL) {
        *pstack = pe->next;
        res = pe->env;
        free(pe);
    }

    return res;
}

__attribute__((unused))
static jmp_buf* jmp_stack_top(jmp_stack_t** pstack)
{
    jmp_buf* res = NULL;

    __assert_not_null(pstack);

    if (*pstack != NULL) {
        res = (*pstack)->env;
    }

    return res;
}

static void __jmp_stack_key_destroy(void* ptr)
{
    jmp_stack_t* stack = (jmp_stack_t*) ptr;
    jmp_buf* env;

    while (stack != NULL) {
        env = jmp_stack_pop(&stack);
        // env should never be NULL here
        __assert_not_null(env);
        free(env);
    }
}

static void __jmp_stack_key_init()
{
    int rc;
    rc = pthread_key_create(&__jmp_stack_key, __jmp_stack_key_destroy);
    __assert_zero(rc);
}

// -- thread local utilities --

jmp_buf* env_create_tl()
{
    jmp_buf* res;
    jmp_stack_t* stack;
    int rc;

    rc = pthread_once(&__jmp_stack_key_init_once, __jmp_stack_key_init);
    __assert_zero(rc);

    res = (jmp_buf*) malloc(sizeof(jmp_buf));
    __assert_not_null(res);

    stack = (jmp_stack_t*) pthread_getspecific(__jmp_stack_key);

    jmp_stack_push(&stack, res);

    rc = pthread_setspecific(__jmp_stack_key, stack);
    __assert_zero(rc);

    return res;
}

jmp_buf* env_remove_tl()
{
    jmp_buf* res;
    jmp_stack_t* stack;
    int rc;

    rc = pthread_once(&__jmp_stack_key_init_once, __jmp_stack_key_init);
    __assert_zero(rc);

    stack = (jmp_stack_t*) pthread_getspecific(__jmp_stack_key);

    res = jmp_stack_pop(&stack);

    rc = pthread_setspecific(__jmp_stack_key, stack);
    __assert_zero(rc);

    return res;
}
