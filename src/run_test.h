/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RUN_TEST_H
#define RUN_TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PRINT_ARRAY(array, array_len, format) \
    {\
        size_t idx;\
        printf(#array " = [");\
        for (idx = 0; idx < array_len; idx++) {\
            if (idx > 0) {\
                printf(", ");\
            }\
            printf(format, array[idx]);\
        }\
        printf("]");\
    }

#define PRINT_RUN_STATUS(passed) printf(" -- %s\n", passed ? "PASSED" : "FAILED");

extern sigjmp_buf __jenv;

#define RUN_TEST(function) \
    { \
        printf("running %s ... ", #function); \
        if (sigsetjmp(__jenv, 1) == 0) { \
            int status = function(); \
            PRINT_RUN_STATUS(status) \
        } else { \
            printf(" -- !!! got segmentation fault !!!\n"); \
        } \
    }

#ifdef __cplusplus
}
#endif

#endif /* RUN_TEST_H */
