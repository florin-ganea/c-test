/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "try_catch.h"
#include "logging.h"

// -- my testing code --

#define ERR_1 1

void my_fun_throw_int(int arg) // throws ERR_1, 100, 200
{
    if (arg == 0) {
        __error("got invalid: %d", arg);
        THROW(ERR_1);
    }

    if (arg == 1) {
        __error("got another invalid: %d", arg);
        THROW(100);
    }

    if (arg == 2) {
        __error("got second another invalid: %d", arg);
        THROW(200);
    }
    
    __debug("got correct: %d", arg);
}

void my_fun_throw(int arg) // throws ERR_1, 100
{
    TRY
        my_fun_throw_int(arg);
    
    CATCH(ERR_1)
        __error("ERR_1 was thrown; throwing up");
        THROW(ERR_1);
    
    CATCH(200) 
        __error("200 was thrown; ignoring");
        
    TRY_END
}

void my_fun(int arg)
{
    TRY
        // normal code
        my_fun_throw(arg);

    CATCH(ERR_1)
        // ERR_1 was thrown
        __error("ERR_1 was thrown");
        // uncomment the line below to test unhandled exception abort
        // THROW(ERR_1);

    CATCH_OTHER
        // all other errors
        __error("other error (%d) was thrown", THROWN);

    TRY_END
}

int main(int argc, char** argv)
{
    my_fun(2);
    my_fun(0);
    my_fun(4);
    my_fun(1);
    my_fun(99);

    return EXIT_SUCCESS;
}
