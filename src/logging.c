/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/time.h>

#include "logging.h"

// TODO make a queue, format and publish on different threads
// TODO expose in configuration the format and the level

#define DT_SIZE 32

static __level_t __level = ALWAYS;

static const char* const LEVEL_NAME[] = {"OFF", "ERROR", "WARNING", "INFO", "DEBUG", "TRACE", "ALWAYS"};
static pthread_mutex_t __log_mutex = PTHREAD_MUTEX_INITIALIZER;

void __log(FILE* storage, __level_t level, const char* logger, const char* src, int line, int indent, const char* fmt, ...)
{
    FILE* lstorage = stdout;
    const char* llogger = "default";

    if (logger != NULL) {
        llogger = logger;
    }

    if (storage != NULL) {
        lstorage = storage;
    }

    pthread_mutex_lock(&__log_mutex);

    if (__level != OFF && level <= __level) {
        char datetime[DT_SIZE] = "";
        int i;
        struct tm loctime;
        struct timeval tv;
        va_list va;

        // TODO handle possible error cases       

        gettimeofday(&tv, NULL);
        localtime_r(&tv.tv_sec, &loctime);
        strftime(datetime, DT_SIZE, "%F@%T", &loctime);

        fprintf(lstorage, "%s.%03d" LOG_SEP "%7s" LOG_SEP "[%s]" LOG_SEP "(%s:%d)" LOG_SEP,
                datetime, (int)(tv.tv_usec / 1000), LEVEL_NAME[level], llogger, src, line);
        for (i = 0; i < indent; i++) {
            fprintf(lstorage, LOG_INDENT);
        }

        va_start(va, fmt);
        vfprintf(lstorage, fmt, va);
        va_end(va);

        fprintf(lstorage, LOG_EOL);
        fflush(lstorage);
    }

    pthread_mutex_unlock(&__log_mutex);
}
