/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>

#include "run_test.h"

//
// -- missing const
//
int non_const_function(char* str, size_t str_len)
{
    str[str_len / 2] = '\0';
    return 0;
}

int const_function(const char* str __attribute((unused)),
                   size_t str_len __attribute((unused)))
{
    // compile error when attempted
    // str[str_len / 2] = '\0';
    return 0;
}

int test_missing_const_ok()
{
    int correct = 1;
    char test[] = "tests";

    non_const_function(test, 6);
    correct = strlen(test) == 3;
    printf("test = %s", test);

    return correct;
}

int test_missing_const_bad()
{
    int correct = 1;
    char* test = "tests";

    non_const_function(test, 6);

    correct = strlen(test) == 3;
    printf("test = %s", test);

    return correct;
}

int test_const()
{
    int correct = 1;

    const_function("tests", 6);

    correct = strlen("tests") == 5;
    printf("tests");

    return correct;
}

/*
 *
 */
int main_01(int argc __attribute((unused)), char** argv __attribute((unused)))
{

    RUN_TEST(test_const)
    RUN_TEST(test_missing_const_ok)
    RUN_TEST(test_missing_const_bad) // here core dump is expected

    return EXIT_SUCCESS;
}
