/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * The purpose of the test is to show that whenever a signal is caused by
 * an action of some thread, the signal is dispatched to that particular
 * thread, and also the signal handler is executed on that same particular 
 * thread.
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

#include "assert.h"
#include "logging.h"

#define WK_COUNT 100

void sigsegv_handler()
{
    __error("got SIGSEGV");
    abort();
}

void sigabrt_handler()
{
    __error("got SIGABRT");
}

void signal_init(void )
{
    struct sigaction sih;
    struct sigaction siha;

    sih.sa_handler = sigsegv_handler;
    sigemptyset(&sih.sa_mask);
    sih.sa_flags = 0;
    sigaction(SIGSEGV, &sih, NULL);

    siha.sa_handler = sigabrt_handler;
    sigemptyset(&siha.sa_mask);
    siha.sa_flags = 0;
    sigaction(SIGABRT, &siha, NULL);
}

void* th_sleepy(void* arg)
{
    long rn;
    int i, rc;
    
    __debug("starting sleepy thread %s", (char*) arg);

    for (i = 0; i < 100000; i++) {
        rn = random() % 100;
        rc = usleep(rn * 1000);
        __assert_zero(rc);
    }

    __debug("stopping sleepy thread %s", (char*) arg);   
    free(arg);
    return NULL;
}

void* th_rand_bad(void* arg)
{
    long rn;
    int rc;
    
    __debug("starting randomly bad thread");

    while (1) {
        rn = random() % 100;
        if (rn == 66) {
            // do a nasty thing
            snprintf(NULL, 1000, ":-p");
        } else {
            rc = usleep(rn * 1000);
            __assert_zero(rc);
        }
    }
    
    __debug("stopping randomly bad thread");   
    return NULL;
}

char* intstr(int val)
{
    char tmp[1024];
    char* rval;
    snprintf(tmp, 1024, "%d", val);
    rval = strdup(tmp);
    return rval;
}

int main(int argc, char** argv) {
    pthread_t th_bad;
    pthread_t th_workers[WK_COUNT];
    int i, rc;
    
    signal_init();
    
    for(i = 0; i < WK_COUNT; i++) {
        rc = pthread_create(&th_workers[i], NULL, th_sleepy, intstr(i));
        __assert_zero(rc);
    }
    
    rc = pthread_create(&th_bad, NULL, th_rand_bad, NULL);
    __assert_zero(rc);

    for(i = 0; i < WK_COUNT; i++) {
        rc = pthread_join(th_workers[i], NULL);
        __assert_zero(rc);
    }

    rc = pthread_join(th_bad, NULL);
    __assert_zero(rc);

    return (EXIT_SUCCESS);
}

